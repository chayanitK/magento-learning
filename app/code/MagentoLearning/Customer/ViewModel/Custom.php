<?php
namespace MagentoLearning\Customer\ViewModel;

//must implement
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Custom implements ArgumentInterface
{
    public function __construct() {

    }

    public function getSomething()
    {
        return "Hello World";
    }
}